import React, { Component } from 'react';
import TableContainer from './containers/TableContainer';
import VisibilitySelector from './containers/VisibilitySelector';
import AttributesContainer from './containers/AttributesContainer';
import './App.css';

import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">Robot List Assignment</h1>
          </header>
          <Switch>
            <Route exact path='/' render={() => <div><VisibilitySelector/><TableContainer/></div>} />
            <Route path='/robot/attributes' render={() => <AttributesContainer/>} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;