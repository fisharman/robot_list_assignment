import axios from 'axios';

const baseUrl = `https://6i9nu7ctv0.execute-api.us-west-2.amazonaws.com/test/robots`;

export const SET_CURRENT_ROBOT = 'SET_CURRENT_ROBOT';
export const REQUEST_ROBOTS = 'REQUEST_ROBOTS';
export const REQUEST_ROBOTS_ERROR = 'REQUEST_ROBOTS_ERROR';
export const RECEIVE_ROBOTS = 'RECEIVE_ROBOTS';
export const SET_VISIBILITY = 'SET_VISIBILITY';
export const VisibilityFilters = {
    SHOW_ALL : 'SHOW_ALL',
    SHOW_ONLINE : 'SHOW_ONLINE',
    SHOW_OFFLINE : 'SHOW_OFFLINE'
};

const setCurrentRobot = thingId => ({
    type: SET_CURRENT_ROBOT,
    thingId
})

const requestRobots = () => ({
    type: REQUEST_ROBOTS
})

const requestRobotsError = err => ({
    type: REQUEST_ROBOTS_ERROR,
    err
})

const receiveRobots = res => ({
    type: RECEIVE_ROBOTS,
    res
})

const setVisibility = filter => ({
    type: SET_VISIBILITY,
    filter
})

const fetchAPI = () => {
    return dispatch => {
        dispatch(requestRobots())
        return axios.get(baseUrl).then(res => {
            dispatch(receiveRobots(res.data));
        }).catch(err => {
            console.log("error:", err)
            dispatch(requestRobotsError(err));
        })
    }
}

export {
    setCurrentRobot,
    requestRobots,
    requestRobotsError,
    fetchAPI,
    setVisibility
}