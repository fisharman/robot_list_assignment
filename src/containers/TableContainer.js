import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from '../components/LoadingSpinner';
import Error from '../components/Error';
import Table from '../components/Table';
import { fetchAPI, VisibilityFilters, setCurrentRobot } from '../actions';

const mapStateToProps = state => ({
    robotData: state.api,
    loading: state.loading,
    error: state.error,
    visibility: state.current_visibility
})

const mapDispatchToProps = dispatch => ({
    fetchRobots: () => dispatch(fetchAPI()),
    handleClick: thingID => dispatch(setCurrentRobot(thingID))
})

class TableContainer extends Component {
    componentDidMount(){
        this.props.fetchRobots()
    }

    render(){
        if (this.props.loading){
            return <LoadingSpinner/>
        }

        if (this.props.error){
            return <Error error={this.props.error}/>
        }

        let toShow = {};
        if (this.props.visibility === VisibilityFilters.SHOW_ALL){
            toShow = this.props.robotData
        }else{
            let status = this.props.visibility === VisibilityFilters.SHOW_ONLINE ?
            'online' : 'offline';
            Object.keys(this.props.robotData).forEach(key => {
                if (this.props.robotData[key].status === status){
                    toShow[key] = this.props.robotData[key];
                }
            })
        }
        return <Table robotData={toShow} onClick={this.props.handleClick}/>
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TableContainer);