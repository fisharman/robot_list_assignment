import React from 'react'
import { connect } from 'react-redux';
import { setVisibility, VisibilityFilters } from '../actions'
import Link from '../components/Link';

const mapStateToProps = state => ({
  current_visibility: state.current_visibility
});

const mapDispatchToProps = dispatch => ({
  onClick: filter => dispatch(setVisibility(filter))
});

const VisibilitySelector = props => (
  <div>
    <span>Show: </span>
    <Link
      active={VisibilityFilters.SHOW_ALL === props.current_visibility}
      onClick={() => props.onClick(VisibilityFilters.SHOW_ALL)}
    >
      All
    </Link>
    <Link 
      active={VisibilityFilters.SHOW_ONLINE === props.current_visibility}
      onClick={() => props.onClick(VisibilityFilters.SHOW_ONLINE)}
    >
      Online
    </Link>
    <Link
      active={VisibilityFilters.SHOW_OFFLINE === props.current_visibility}
      onClick={() => props.onClick(VisibilityFilters.SHOW_OFFLINE)}
    >  
      Offline
    </Link>
  </div>
)

export default connect(mapStateToProps, mapDispatchToProps)(VisibilitySelector);
