import React from 'react';
import { connect } from 'react-redux';
import Attributes from '../components/Attributes';
//import { fetchAPI, VisibilityFilters } from '../actions';

const mapStateToProps = state => ({
  attributes: state.api[state.currentRobot] ? state.api[state.currentRobot].attributes : ""
});

const AttributesContainer = props => {
    return <Attributes attributes={props.attributes}/>
}

export default connect(mapStateToProps)(AttributesContainer);