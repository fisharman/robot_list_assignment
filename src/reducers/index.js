import { combineReducers } from 'redux';

import {
    SET_CURRENT_ROBOT,
    REQUEST_ROBOTS,
    REQUEST_ROBOTS_ERROR,
    RECEIVE_ROBOTS,
    SET_VISIBILITY,
    VisibilityFilters
} from '../actions';

const createMapping = data => {
    const out = {}
    if (data){
        data.forEach(robot => {
            out[robot.thingId] = robot
        })
    }
    return out;
}

const currentRobot = (state = '', action) => {
    switch(action.type) {
        case SET_CURRENT_ROBOT:
            return action.thingId
        //case REQUEST_ROBOTS:
        //case REQUEST_ROBOTS_ERROR:
        //case RECEIVE_ROBOTS:
        //case SET_VISIBILITY:
        default:
            return state;
    }
}

const api = (state = {}, action) => {
    switch (action.type) {
        //case SET_CURRENT_ROBOT:
        //case REQUEST_ROBOTS:
        //case REQUEST_ROBOTS_ERROR:
        case RECEIVE_ROBOTS:
            return {
                ...state,
                ...createMapping(action.res)
            }
        //case SET_VISIBILITY:    
        default:
            return state;
 
    }
}

const loading = (state = false, action) => {
    switch(action.type) {
        //case SET_SELECTED_ROBOT:
        case REQUEST_ROBOTS:
            return true;
        case REQUEST_ROBOTS_ERROR:
            return false;
        case RECEIVE_ROBOTS:
            return false;
        //case SET_VISIBILITY:
        default:
            return state;
    }
}

const error = (state = '', action) => {
    switch(action.type) {
        //case SET_CURRENT_ROBOT:
        //case REQUEST_ROBOTS:
        case REQUEST_ROBOTS_ERROR:
            return action.err.message;
        //case RECEIVE_ROBOTS:
        //case SET_VISIBILITY:
        default:
            return state;
    }
}

const current_visibility = (state = VisibilityFilters.SHOW_ALL, action) => {
    switch (action.type) {
        //case CURRENT_ROBOT:
        //case REQUEST_ROBOTS:
        //case REQUEST_ROBOTS_ERROR:
        //case RECEIVE_ROBOTS:
        case SET_VISIBILITY:
            return action.filter;
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    currentRobot,
    api,
    loading,
    error,
    current_visibility
});

export default rootReducer;

// retrieve currentChapter from json file.
/*
const initialState = {
    currentRobot: 'thingId'
    api: {
        'thingId': {thing object},
        ...
    },
    loading: false,
    error: '',
    visibilityFilter: all
}
*/