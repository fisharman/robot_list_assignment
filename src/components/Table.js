import React from 'react';
import './Table.css';
import { Link } from 'react-router-dom';

const Table = props => {
  return(
    <div className="container">
      <table className="table table-hover table-bordered">
        <thead className="thead-light">
          <tr>
            <th scope="col">Status</th>
            <th scope="col">Name</th>
            <th scope="col">Version</th>
            <th scope="col">Thing ID</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(props.robotData).map(key => {
            return (
              <Row
                key={key}
                {...props.robotData[key]}
                attributes={'attributes'}
                onClick={() => props.onClick(key)}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

const Row = props => {
  let cellStyle = props.status === "online" ? "table-success" : "table-warning";
  return(
    <tr>
      <td className={cellStyle}>{props.status}</td>
      <td>{props.thingName}</td>
      <td>{props.version}</td>
      <td>{props.thingId}</td>
      <td><button className="btn btn-outline-primary" onClick={props.onClick}><Link to='/robot/attributes'>{props.attributes}</Link></button></td>
    </tr>
  );
};

export default Table;
