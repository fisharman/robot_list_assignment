import React from 'react';

const Error = props => {
  if (!props.error)
    return null;

  return (
    <div className="alert alert-danger">{props.error || ""}. Reload the Page.</div>
  )
};

export default Error;
