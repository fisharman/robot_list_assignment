import React from 'react';

const Attributes = props => {
    return(
        <div className="container">
            <table className="table table-hover table-bordered">
                <thead className="thead-light">
                <tr>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                </tr>
                </thead>
                <tbody>
                {Object.keys(props.attributes).map(key => {
                    return (
                    <Row
                        key={key}
                        attribute={key}
                        value={props.attributes[key]}
                    />
                    );
                })}
                </tbody>
            </table>
        </div>
    );
}

const Row = props => {
    return(
    <tr>
        <td>{props.attribute}</td>
        <td>{props.value}</td>
    </tr>
    );
};

export default Attributes;